//create a local connection
const localConnection = new RTCPeerConnection();
//create a data channel
const dataChannel = localConnection.createDataChannel('channel');
//print this whenever I got a message
dataChannel.onmessage = event => console.log('Just got a message ' + event.data);
//print this when a connection is opened
dataChannel.onopen = event => console.log('Connection opened');
//Let's print the SDP every time a new ICE candidate is detected
localConnection.onicecandidate = event => {
	console.log(
		'New ICE candidate! Reprinting SDP' +
			JSON.stringify(localConnection.localDescription),
	);
	//the 'localDescription' is the local SDP
};
//create an offer (the offer is our SDP) and set that offer in the localDescription
localConnection
	.createOffer()
	.then(offer => localConnection.setLocalDescription(offer)
    .then(a => console.log('set succesfully!')));

//=====================================================
//This section needs to be filled after Browser B created its SDP
//=====================================================

//the answer is the SDP that Browser B created
const answer = {"type":"answer","sdp":"v=0\r\no=- 8674876445196826157 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0\r\na=msid-semantic: WMS\r\nm=application 9 UDP/DTLS/SCTP webrtc-datachannel\r\nc=IN IP4 0.0.0.0\r\na=candidate:1880550016 1 udp 2113937151 3e4b8bd5-b20d-4056-88ef-9d0f73a85df2.local 53337 typ host generation 0 network-cost 999\r\na=ice-ufrag:yJlE\r\na=ice-pwd:9YqLBwD9BJTei62GWUO8A0+f\r\na=ice-options:trickle\r\na=fingerprint:sha-256 F2:EA:3F:0D:E7:E3:C1:94:32:B8:C3:74:C2:0E:75:B7:4C:59:7E:81:E7:EF:68:CE:6C:05:9A:00:AB:0C:5D:5F\r\na=setup:active\r\na=mid:0\r\na=sctp-port:5000\r\na=max-message-size:262144\r\n"}
//FINALLY
//this will open the connection in both browsers
localConnection.setRemoteDescription(answer);

//=====================================================
//After connection is established
//=====================================================

//we can now communicate the browsers via the data channel
dataChannel.send('Hey bro!')

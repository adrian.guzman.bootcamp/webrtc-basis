//this offer is the SDP created by Browser A
const offer = {
	type: 'offer',
	sdp:
		'v=0\r\no=- 6071851695855328350 2 IN IP4 127.0.0.1\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0\r\na=msid-semantic: WMS\r\nm=application 9 UDP/DTLS/SCTP webrtc-datachannel\r\nc=IN IP4 0.0.0.0\r\na=candidate:1880550016 1 udp 2113937151 3e4b8bd5-b20d-4056-88ef-9d0f73a85df2.local 65447 typ host generation 0 network-cost 999\r\na=ice-ufrag:SiOp\r\na=ice-pwd:84dR4Tttkd89fb1a2rkWT8Eb\r\na=ice-options:trickle\r\na=fingerprint:sha-256 96:1D:26:E0:7E:BE:2A:4A:83:8E:8F:8F:77:5E:3C:26:EC:EB:EB:37:EB:4C:0A:87:98:43:B4:80:DF:7D:29:6A\r\na=setup:actpass\r\na=mid:0\r\na=sctp-port:5000\r\na=max-message-size:262144\r\n',
};
//create the "remote" connection
const remoteConnection = new RTCPeerConnection();
//Let's print the SDP every time a new ICE candidate is detected
remoteConnection.onicecandidate = event => {
	console.log(
		'New ICE candidate! Reprinting SDP' +
			JSON.stringify(remoteConnection.localDescription),
	);
	//the 'localDescription' is the local SDP
};
//receive the data channel (note that Browser A created the channel)
remoteConnection.ondatachannel = event => {
	//we now crate our data channel as a property of the remote connection
	//and pass it the channel that we are receiving
	remoteConnection.dataChannel = event.channel;
	remoteConnection.dataChannel.onmessage = event =>
		console.log('new message from client ' + event.data);
	remoteConnection.dataChannel.onopen = event =>
		console.log('connection opened');
};
//set the remote description (remote SDP)
remoteConnection
	.setRemoteDescription(offer)
	.then(a => console.log('offer set'));
//set the local description for this browser
remoteConnection
	.createAnswer()
	.then(a =>
		remoteConnection
			.setLocalDescription(a)
			.then(a => console.log('answer created')),
	);

//=====================================================
//After connection is established
//=====================================================

//we can now communicate the browsers via the data channel
remoteConnection.dataChannel.send("What's up bro?")